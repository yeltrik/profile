@can('viewAny', \Yeltrik\Profile\app\models\Profile::class)
    @if (Route::has('profiles.index'))
        <li class="nav-item {{ Route::currentRouteNamed('profiles.index') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('profiles.index') }}">
                Profiles
            </a>
        </li>
    @endif
@endcan
