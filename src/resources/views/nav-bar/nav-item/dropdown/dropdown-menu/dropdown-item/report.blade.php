@can('report')
    @if (Route::has('profiles.index'))
        <a class="dropdown-item" href="{{ route('profiles.index') }}">Profiles</a>
    @endif
@endcan
