@extends('layouts.app')

@section('content')
    <div class="container">

        @include('profile::profile.card')

        <div class="card mt-2">
            <div class="card-body">

                @include('profile::profile.dependent.nav.nav')

            </div>
        </div>

    </div>
@endsection
