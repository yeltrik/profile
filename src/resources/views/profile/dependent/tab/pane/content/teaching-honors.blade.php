@if($nominee instanceof \Yeltrik\TeachingHonors\app\models\Nominee)
    Nomination Count: {{$nominee->nominations->count()}}
@else
    Unknown
@endif
