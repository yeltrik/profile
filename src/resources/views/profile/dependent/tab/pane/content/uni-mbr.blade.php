@if($member instanceof \Yeltrik\UniMbr\app\models\Member)
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">{{$member->name}}</h5>
            <h6 class="card-subtitle mb-2 text-muted">{{$member->email}}</h6>
        </div>
    </div>
@else
    Unknown
@endif

<hr>

University Membership ...

<p>
    Uni Mbr<br>
    Member & Faculty/ Staff / Student / Department Head / Dean <br>
    with University / College / Department
</p>
