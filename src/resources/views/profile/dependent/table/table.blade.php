<?php $tableId = "profile-table-" . rand(1000, 9999);?>
<script>
    $(document).ready(function () {
        $.noConflict();
        $('#{{$tableId}}').DataTable();
    });
</script>

<table id="{{$tableId}}" class="table table-striped">
    <thead>
    <tr>
        <th scope="col">
            Profile
        </th>
        <th scope="col">
            Names
        </th>
        <th scope="col">
            Emails
        </th>
        <th scope="col">
            PD Rosters
        </th>
        <th scope="col">
            Nominee
        </th>
        <th scope="col">
            University Membership
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($profileDependents as $subArray)
        <?php $profile = $subArray['profile']; ?>
        <?php $emails = $subArray['emails']; ?>
        <?php $rosters = $subArray['rosters']; ?>
        <?php $personalNames = $subArray['personalNames']; ?>
        <?php $member = $subArray['member']; ?>
        <?php $nominee = $subArray['nominee']; ?>
        <?php $nominationCount = $subArray['nomination_count']; ?>
        <tr>
            <td scope="row" class="text-center">
                @include('profile::profile.table.icon')
                {{--@include('profile::profile.dependent.table.profile-card')--}}
            </td>

            @include('profile::profile.table.td.personal-name')
            @include('profile::profile.table.td.email')
            @include('profile::profile.dependent.table.td.pd-psr')
            @include('profile::profile.dependent.table.td.nominee')
            @include('profile::profile.dependent.table.td.uni-mbr')
        </tr>
    @endforeach
    </tbody>
</table>

@include('profile::profile.table.script')
