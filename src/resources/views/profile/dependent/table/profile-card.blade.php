<div class="card" style="width: 18rem;">
    <div class="card-body">
        @include('profile::profile.dependent.table.icon')
        @foreach($personalNames as $personalName)
            <h5 class="card-title">{{$personalName->fullName}}</h5>
        @endforeach
        @foreach($emails as $email)
            <p class="card-text">{{$email->email}}</p>
        @endforeach
    </div>
</div>
