<td>
    @if(class_exists(\Yeltrik\TeachingHonors\app\models\Nominee::class))
        @if($nominee instanceof \Yeltrik\TeachingHonors\app\models\Nominee)
            YES
            <span class="badge badge-pill badge-info float-right" title="Nomination Count">
                {{$nominationCount}}
            </span>
        @else
            NO
        @endif
    @else
        Unknown
    @endif
</td>
