<td>
    @if($member instanceof \Yeltrik\UniMbr\app\models\Member)
        <div class="card">
            <h5 class="card-title">
                {{$member->name}}
            </h5>
            <p>
                {{$member->email}}
            </p>
        </div>
    @else
        Unknown
    @endif
</td>
