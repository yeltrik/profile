<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link"
           id="nav-consultation-tab"
           data-toggle="tab"
           href="#nav-consultation"
           role="tab"
           aria-controls="nav-consultation"
           aria-selected="true">Consultation</a>
        <a class="nav-item nav-link"
           id="nav-pd-tab"
           data-toggle="tab"
           href="#nav-pd"
           role="tab"
           aria-controls="nav-pd"
           aria-selected="false">
            Professional Development
            @if($rosters instanceof \Illuminate\Database\Eloquent\Collection)
                <span class="badge badge-pill badge-info">{{$rosters->count()}}</span>
            @endif
        </a>
        <a class="nav-item nav-link"
           id="nav-teaching-honors-tab"
           data-toggle="tab"
           href="#nav-teaching-honors"
           role="tab"
           aria-controls="nav-teaching-honors"
           aria-selected="false">
            Teaching Honors
            @if($nominee instanceof \Yeltrik\TeachingHonors\app\models\Nominee)
                <span class="badge badge-pill badge-info">{{$nominee->nominations->count()}}</span>
            @endif
        </a>
        <a class="nav-item nav-link"
           id="nav-uni-mbr-tab"
           data-toggle="tab"
           href="#nav-uni-mbr"
           role="tab"
           aria-controls="nav-uni-mbr"
           aria-selected="false">University Membership</a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade"
         id="nav-consultation"
         role="tabpanel"
         aria-labelledby="nav-consultation-tab">
        @include('profile::profile.dependent.tab.pane.content.consultation')
    </div>
    <div class="tab-pane fade"
         id="nav-pd"
         role="tabpanel"
         aria-labelledby="nav-pd-tab">
        @include('profile::profile.dependent.tab.pane.content.pd')
    </div>
    <div class="tab-pane fade"
         id="nav-teaching-honors"
         role="tabpanel"
         aria-labelledby="nav-teaching-honors-tab">
        @include('profile::profile.dependent.tab.pane.content.teaching-honors')
    </div>
    <div class="tab-pane fade"
         id="nav-uni-mbr"
         role="tabpanel"
         aria-labelledby="nav-uni-mbr-tab">
        @include('profile::profile.dependent.tab.pane.content.uni-mbr')
    </div>
</div>
