@extends('layouts.app')

@section('content')
    <div class="container">

        @include('profile::profile.table.index')

        <a href="{{route('profiles.dependents.index')}}" class="btn btn-primary btn-block mt-2">
            View with Dependents
        </a>

    </div>
@endsection
