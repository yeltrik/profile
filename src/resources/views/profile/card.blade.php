<div class="card">
    <div class="card-body">
        @foreach($personalNames as $personalName)
            <h5 class="card-title">{{$personalName->fullName}}</h5>
        @endforeach

        @foreach($corporateTitles as $corporateTitle)
            <h6 class="card-title">{{$corporateTitle->title}}</h6>
        @endforeach

        @foreach($emails as $email)
            <h6 class="card-subtitle mb-2 text-muted">{{$email->email}}</h6>
        @endforeach
    </div>
</div>
