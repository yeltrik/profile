<script>
    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.getAttribute('data-profile_id'));
    }

    function drop(ev) {
        ev.preventDefault();
        var dragId = ev.dataTransfer.getData("Text");
        var targetId = ev.target.getAttribute('data-profile_id');
        let route = "{{ route('profiles.index') }}";
        let count = "#";
        let from = dragId;
        let to = targetId;
        $message = "Are you Sure you want to Merge Profiles?";
        var r = confirm($message);
        if (r == true) {
            window.open(route + "/" + dragId + "/merge/" + targetId);
        }
    }
</script>
