<a href="{{route('profiles.show', $profile)}}">
    @can('mergeAny', \Yeltrik\Profile\app\models\Profile::class)
        <i
            class="far fa-user btn btn-primary float-left"
            data-profile_id="{{$profile->id}}"
            draggable="true"
            ondragstart="drag(event)"
            ondrop="drop(event)"
            ondragover="allowDrop(event)"
        ></i>
    @else
        <i
            class="far fa-user btn btn-primary float-left"
        ></i>
    @endcan
</a>
