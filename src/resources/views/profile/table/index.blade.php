<?php $tableId = "profile-table-" . rand(1000, 9999);?>
<script>
    $(document).ready(function () {
        $.noConflict();
        $('#{{$tableId}}').DataTable();
    });
</script>

<table id="{{$tableId}}" class="table table-striped">
    <thead>
    <tr>
        <th scope="col">
            Profile
        </th>
        <th scope="col">
            Names
        </th>
        <th scope="col">
            Emails
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($profiles as $profile)
        <?php $emails = $profile->emails; ?>
        <?php $personalNames = $profile->personalNames; ?>
        <tr>
            <td scope="row" class="text-center">
                @include('profile::profile.table.icon')
                {{--@include('profile::profile.dependent.table.profile-card')--}}
            </td>

            @include('profile::profile.table.td.personal-name')
            @include('profile::profile.table.td.email')
        </tr>
    @endforeach
    </tbody>
</table>

@include('profile::profile.table.script')
