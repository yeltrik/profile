<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Profile\app\http\controllers\PersonalNameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('profile/personal-name/',
    [PersonalNameController::class, 'index'])
    ->name('profiles.personalNames.index');

Route::get('profile/personal-name/{personalName}',
    [PersonalNameController::class, 'show'])
    ->name('profiles.personalNames.show');
