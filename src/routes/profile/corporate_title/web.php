<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Profile\app\http\controllers\CorporateTitleController;
use Yeltrik\Profile\app\http\controllers\PersonalNameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('profile/corporate-title/',
    [CorporateTitleController::class, 'index'])
    ->name('profiles.corporateTitles.index');

Route::get('profile/corporate-title/{corporateTitle}',
    [CorporateTitleController::class, 'show'])
    ->name('profiles.corporateTitles.show');
