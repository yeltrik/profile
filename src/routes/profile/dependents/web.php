<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Profile\app\http\controllers\ProfileDependentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('profile/dependent',
    [ProfileDependentController::class, 'index'])
    ->name('profiles.dependents.index');
