<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Profile\app\http\controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'corporate_title/web.php';
require 'dependents/web.php';
require 'email/web.php';
require 'personal_name/web.php';

Route::get('profile/',
    [ProfileController::class, 'index'])
    ->name('profiles.index');

Route::get('profile/{profile}',
    [ProfileController::class, 'show'])
    ->name('profiles.show');

Route::get('profile/{profileFrom}/merge/{profileTo}',
    [ProfileController::class, 'merge'])
    ->name('profiles.dependents.merge');
