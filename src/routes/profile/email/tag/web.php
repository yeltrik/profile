<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Profile\app\http\controllers\EmailTagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('profile/email/tag',
    [EmailTagController::class, 'index'])
    ->name('profiles.emails.tags.index');

Route::get('profile/email/tag/{tag}',
    [EmailTagController::class, 'show'])
    ->name('profiles.emails.tags.show');
