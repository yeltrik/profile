<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Profile\app\http\controllers\EmailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'tag/web.php';

Route::get('profile/email/',
    [EmailController::class, 'index'])
    ->name('profiles.emails.index');

Route::get('profile/email/{email}',
    [EmailController::class, 'show'])
    ->name('profiles.emails.show');
