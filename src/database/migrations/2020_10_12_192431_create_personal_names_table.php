<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('profile')->create('personal_names', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('profile_id');
            $table->string('title')->nullable();
            $table->string('first');
            $table->string('middle')->nullable();
            $table->string('last');
            $table->string('suffix')->nullable();
            $table->timestamps();

            $table->unique(['profile_id', 'first', 'last']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('profile')->dropIfExists('personal_names');
    }
}
