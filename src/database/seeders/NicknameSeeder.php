<?php

namespace Yeltrik\Profile\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\Profile\app\models\Nickname;
use Yeltrik\Profile\app\models\Profile;

class NicknameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nicknames = Nickname::factory()
            ->count(3)
            ->for(Profile::factory())
            ->create();
    }
}
