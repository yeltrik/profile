<?php

namespace Yeltrik\Profile\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;

class PersonalNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personalNames = PersonalName::newFactory()
            ->count(1)
            ->for(Profile::factory())
            ->create();
    }
}
