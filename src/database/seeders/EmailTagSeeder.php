<?php

namespace Yeltrik\Profile\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\EmailTag;
use Yeltrik\Profile\app\models\Profile;

class EmailTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emailTags = EmailTag::factory()
            ->count(1)
            ->for(Email::factory()->for(Profile::factory()))
            ->create();
    }
}
