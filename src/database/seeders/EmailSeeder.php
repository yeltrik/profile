<?php

namespace Yeltrik\Profile\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\Profile;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emails = Email::factory()
            ->count(3)
            ->for(Profile::factory())
            ->create();
    }
}
