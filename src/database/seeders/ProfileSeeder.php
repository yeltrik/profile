<?php

namespace Yeltrik\Profile\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\Profile\app\models\Profile;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profiles = Profile::factory()
            ->count(3)
            ->create();
    }
}
