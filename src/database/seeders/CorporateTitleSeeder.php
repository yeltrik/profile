<?php

namespace Yeltrik\Profile\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\Profile\app\models\CorporateTitle;
use Yeltrik\Profile\app\models\Profile;

class CorporateTitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $corporateTitles = CorporateTitle::factory()
            ->count(3)
            ->for(Profile::factory())
            ->create();
    }
}
