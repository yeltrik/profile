<?php

namespace Yeltrik\Profile\database\factories;

use Yeltrik\Profile\app\models\PersonalName;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonalNameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PersonalName::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $genders = ['male', 'female'];
        $gender = $genders[rand(0,1)];
        return [
            'title' => $this->faker->title($gender),
            'first' => $this->faker->firstName($gender),
            'last' => $this->faker->lastName
        ];
    }
}
