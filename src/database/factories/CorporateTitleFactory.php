<?php

namespace Yeltrik\Profile\database\factories;

use Yeltrik\Profile\app\models\CorporateTitle;
use Illuminate\Database\Eloquent\Factories\Factory;

class CorporateTitleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CorporateTitle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word
        ];
    }
}
