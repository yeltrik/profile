<?php

namespace Yeltrik\Profile\database\factories;

use Yeltrik\Profile\app\models\EmailTag;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmailTagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmailTag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tag' => $this->faker->word()
        ];
    }
}
