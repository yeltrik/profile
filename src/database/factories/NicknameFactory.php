<?php

namespace Yeltrik\Profile\database\factories;

use Yeltrik\Profile\app\models\Nickname;
use Illuminate\Database\Eloquent\Factories\Factory;

class NicknameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Nickname::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nickname' => $this->faker->firstName
        ];
    }
}
