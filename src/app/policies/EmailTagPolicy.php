<?php

namespace Yeltrik\Profile\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmailTagPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny()
    {
        return TRUE;
    }

    public function view()
    {
        return TRUE;
    }

}
