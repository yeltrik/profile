<?php

namespace Yeltrik\Profile\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfilePolicy
{
    const ADMIN_EMAILS_ENV_KEY = "ADMIN_EMAILS";

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny()
    {
        return TRUE;
    }

    public function view()
    {
        return TRUE;
    }

    public function mergeAny(User $user) {
        $emails = explode(",", env(static::ADMIN_EMAILS_ENV_KEY));
        if (empty($emails)) {
            dd('Need to Set ' . static::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
        } else {
            $email = $user->email;
            return in_array(strtolower($email), $emails);
        }
        return FALSE;
    }

}
