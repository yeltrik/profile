<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\Profile\app\models\EmailTag;
use Illuminate\Http\Request;

class EmailTagController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailTag  $emailTag
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTag $emailTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmailTag  $emailTag
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailTag $emailTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmailTag  $emailTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailTag $emailTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailTag  $emailTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTag $emailTag)
    {
        //
    }
}
