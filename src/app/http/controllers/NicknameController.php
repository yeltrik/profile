<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\Profile\app\models\Nickname;
use Illuminate\Http\Request;

class NicknameController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Nickname::class);

        $nicknames = Nickname::all();
    }

    /**
     * @param Nickname $nickname
     * @throws AuthorizationException
     */
    public function show(Nickname $nickname)
    {
        $this->authorize('view', $nickname);

        $profile = $nickname->profile;
    }

}
