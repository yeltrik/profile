<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\Profile\app\models\PersonalName;
use Illuminate\Http\Request;

class PersonalNameController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', PersonalName::class);

        $personalNames = PersonalName::all();
    }

    /**
     * @param PersonalName $personalName
     * @throws AuthorizationException
     */
    public function show(PersonalName $personalName)
    {
        $this->authorize('view', $personalName);

        $profile = $personalName->profile;
    }

}
