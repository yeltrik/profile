<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\Profile\app\models\Profile;
use Illuminate\Http\Request;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\UniMbr\app\models\Member;

class ProfileDependentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Profile::class);

        $profiles = Profile::all();


        $profileDependents = [];
        foreach ($profiles as $profile) {
            $subArray = [
                'profile' => $profile,
                'emails' => $profile->emails,
                'rosters' => NULL,
                'personalNames' => $profile->personalNames,
                'member' => NULL,
                'nominee' => NULL,
                'nomination_count' => NULL,
            ];
            // TODO: This might not be included in the project
            if (class_exists(ProfileUniMbr::class)) {
                if (class_exists(Member::class)) {
                    $member = Member::query()
                        ->whereIn('id', ProfileUniMbr::query()
                            ->where('profile_id', '=', $profile->id)
                            ->pluck('member_id')
                            ->toArray()
                        )
                        ->first();
                    $subArray['member'] = $member;
                }
            }

            if ( class_exists(Roster::class)) {
                $rosters = Roster::query()
                    ->where('profile_id', '=', $profile->id)
                    ->get();
                $subArray['rosters'] = $rosters;
            }

            if ( class_exists(Nominee::class)) {
                $nominee = Nominee::query()
                    ->where('profile_id', '=', $profile->id)
                    ->first();
                $subArray['nominee'] = $nominee;
                if ( $nominee instanceof Nominee ) {
                    $subArray['nomination_count'] = $nominee->nominations->count();
                }
            }
            $profileDependents[] = $subArray;
        }

        return view('profile::profile.dependent.index', compact([
            'profileDependents'
        ]));
    }

}
