<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\Profile\app\models\Email;
use Illuminate\Http\Request;

class EmailController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Email::class);

        $emails = Email::all();
    }


    /**
     * @param Email $email
     * @throws AuthorizationException
     */
    public function show(Email $email)
    {
        $this->authorize('view', $email);

        $profile = $email->profile;
    }

}
