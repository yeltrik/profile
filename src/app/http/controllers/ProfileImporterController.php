<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\Profile\app\import\ProfileImporter;
use Yeltrik\Profile\app\models\Profile;
use Illuminate\Http\Request;

class ProfileImporterController extends Controller
{

    /**
     * ProfileImporterController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Profile::class);

    }

    /**
     * @param Request $request
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Profile::class);

        $email = $request->email;
        $first = $request->first;
        $last = $request->last;

        $profileImporter = new ProfileImporter();
        $profileImporter->associateWithEmailStr($email);
        $profileImporter->associateWithPersonalNameStr($first, $last);
        $profile = $profileImporter->profile(TRUE);

        dd($profile);
    }

}
