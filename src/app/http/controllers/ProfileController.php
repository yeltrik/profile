<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Illuminate\Http\Request;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\UniMbr\app\models\Member;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Profile::class);

        $profiles = Profile::all();

        return view('profile::profile.index', compact([
            'profiles'
        ]));
    }

    /**
     * @param Profile $profile
     * @throws AuthorizationException
     */
    public function show(Profile $profile)
    {
        $this->authorize('view', $profile);

        $corporateTitles = $profile->corporateTitles;
        $emails = $profile->emails;
        $personalNames = $profile->personalNames;

        $consultations = $this->consultations($profile);
        $nominee = $this->nominee($profile);
        $rosters = $this->rosters($profile);
        $member = $this->member($profile);

        return view('profile::profile.show', compact([
            'profile',
            'corporateTitles',
            'emails',
            'personalNames',
            'consultations',
            'nominee',
            'rosters',
            'member'
        ]));
    }

    /**
     * @param Profile $profileFrom
     * @param Profile $profileTo
     */
    public function merge(Profile $profileFrom, Profile $profileTo)
    {
//        dd([
//            'Warning: Must also move Dependents, or will loose Data Integrity',
//            'Warning: Can we Move All, without Failing?',
//            $profileFrom,
//            $profileTo
//        ]);

        // TODO: Try catch to move Dependents, before re-associating Profiles, and Deleting
        // External Dependents

        // Local Dependents
        foreach ($profileFrom->corporateTitles as $corporateTitle) {
            $corporateTitle->profile()->associate($profileTo);
            $corporateTitle->save();
        }
        foreach ($profileFrom->emails as $email) {
            $email->profile()->associate($profileTo);
            $email->save();
        }
        foreach ($profileFrom->personalNames as $personalName) {
            $personalNameTo = PersonalName::query()
                ->where('profile_id', $profileTo->id)
                ->where('first', '=', $personalName->first)
                ->where('last', '=', $personalName->last);
            if ( $personalNameTo->exists() ) {
                $personalName->delete();
            } else {
                $personalName->profile()->associate($profileTo);
                $personalName->save();
            }
        }

        if (class_exists(ProfileUniMbr::class)) {
            $profileUniMbrFrom = ProfileUniMbr::query()
                ->where('profile_id', '=', $profileFrom->id)
                ->first();
            if ($profileUniMbrFrom instanceof ProfileUniMbr) {
                $profileUniMbrTo = ProfileUniMbr::query()
                    ->where('profile_id', '=', $profileTo->id)
                    ->where('member_id', '=', $profileUniMbrFrom->member->id)
                    ->first();
                if ( $profileUniMbrTo instanceof ProfileUniMbr ) {
                    $profileUniMbrFrom->delete();
                } else {
                    $profileUniMbrFrom->profile()->associate($profileTo);
                    $profileUniMbrFrom->save();
                }
            }
        }

        $nomineeFrom = $this->nominee($profileFrom);
        if ($nomineeFrom instanceof Nominee) {
            $nomineeTo = Nominee::query()
                ->where('profile_id', '=', $profileTo->id);
            if ( $nomineeTo->exists() ) {
                foreach($nomineeFrom->nominations as $nominationFrom) {
                    $nominationFrom->nominee()->associate($nomineeTo->first());
                    $nominationFrom->save();
                }
                $nomineeFrom->delete();
            } else {
                $nomineeFrom->profile()->associate($profileTo);
                $nomineeFrom->save();
            }
        }

        $rosters = $this->rosters($profileFrom);
        if ($rosters instanceof Collection) {

            foreach ($rosters as $roster) {
                $roster->profile()->associate($profileTo);
                $roster->save();
            }
        }

        $profileFrom->delete();

        return redirect()->route('profiles.show', [$profileTo]);
    }

    /**
     * @param Profile $profile
     * @return |null
     */
    public function consultations(Profile $profile)
    {
        return NULL;
    }

    /**
     * @param Profile $profile
     * @return Builder|Model|object|null
     */
    public function member(Profile $profile)
    {
        if (class_exists(ProfileUniMbr::class)) {
            if (class_exists(Member::class)) {
                return Member::query()
                    ->whereIn('id', ProfileUniMbr::query()
                        ->where('profile_id', '=', $profile->id)
                        ->pluck('member_id')
                        ->toArray()
                    )
                    ->first();
            }
        }
        return NULL;
    }

    /**
     * @param Profile $profile
     * @return Builder|Model|object|null
     */
    public function nominee(Profile $profile)
    {
        if (class_exists(Nominee::class)) {
            return Nominee::query()
                ->where('profile_id', '=', $profile->id)
                ->first();
        } else {
            return NULL;
        }
    }

    /**
     * @param Profile $profile
     * @return Builder[]|Collection|null
     */
    public function rosters(Profile $profile)
    {
        if (class_exists(Roster::class)) {
            return Roster::query()
                ->where('profile_id', '=', $profile->id)
                ->get();
        } else {
            return NULL;
        }
    }

}
