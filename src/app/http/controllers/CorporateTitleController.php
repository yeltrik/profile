<?php

namespace Yeltrik\Profile\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\Profile\app\models\CorporateTitle;
use Illuminate\Http\Request;

class CorporateTitleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CorporateTitle  $corporateTitle
     * @return \Illuminate\Http\Response
     */
    public function show(CorporateTitle $corporateTitle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CorporateTitle  $corporateTitle
     * @return \Illuminate\Http\Response
     */
    public function edit(CorporateTitle $corporateTitle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CorporateTitle  $corporateTitle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CorporateTitle $corporateTitle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CorporateTitle  $corporateTitle
     * @return \Illuminate\Http\Response
     */
    public function destroy(CorporateTitle $corporateTitle)
    {
        //
    }
}
