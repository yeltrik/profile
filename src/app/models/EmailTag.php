<?php

namespace Yeltrik\Profile\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\Profile\database\factories\EmailTagFactory;

/**
 * Class EmailTag
 *
 * @property int id
 * @property int email_id
 * @property string tag
 *
 * @property Email email
 *
 * @package Yeltrik\Profile\app\models
 */
class EmailTag extends Model
{
    use HasFactory;

    protected $connection = 'profile';
    public $table = 'email_tags';

    /**
     * @return BelongsTo
     */
    public function email()
    {
        return $this->belongsTo(Email::class);
    }

    /**
     * @return EmailTagFactory
     */
    public static function factory()
    {
        return new EmailTagFactory();
    }
}
