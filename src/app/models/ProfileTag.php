<?php

namespace Yeltrik\Profile\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\Profile\app\models\Profile;

/**
 * Class ProfileTag
 *
 * @property int profile_id
 * @property string tag
 *
 * @package App\Models
 */
class ProfileTag extends Model
{
    use HasFactory;

    protected $connection = 'profile';
    public $table = 'profile_tags';

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

}
