<?php

namespace Yeltrik\Profile\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\Profile\database\factories\PersonalNameFactory;

/**
 * Class PersonalName
 *
 * @property int id
 * @property int profile_id
 * @property string title
 * @property string first
 * @property string middle
 * @property string last
 * @property string suffix
 *
 * @property string fullName
 *
 * @property Profile profile
 *
 * @package Yeltrik\Profile\app\models
 */
class PersonalName extends Model
{
    use HasFactory;

    protected $connection = 'profile';
    public $table = 'personal_names';

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        $pieces = [];
        if ( $this->title != NULL ) {
            $pieces[] = $this->title;
        }
        if ( $this->first != NULL ) {
            $pieces[] = $this->first;
        }
        if ( $this->middle != NULL ) {
            $pieces[] = $this->middle;
        }
        if ( $this->last != NULL ) {
            $pieces[] = $this->last;
        }
        if ( $this->suffix != NULL ) {
            $pieces[] = ',';
            $pieces[] = $this->suffix;
        }
        return implode(" ", $pieces);
    }

    /**
     * @return string
     */
    public function getLastFirstAttribute()
    {
        $pieces = [];
        if ( $this->last != NULL ) {
            $pieces[] = $this->last;
        }
        if ( $this->first != NULL ) {
            $pieces[] = $this->first;
        }
        return implode(", ", $pieces);
    }

    /**
     * @return PersonalNameFactory
     */
    public static function newFactory()
    {
        return new PersonalNameFactory();
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

}
