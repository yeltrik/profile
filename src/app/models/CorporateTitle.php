<?php

namespace Yeltrik\Profile\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\Profile\database\factories\CorporateTitleFactory;

/**
 * Class CorporateTitle
 *
 * @property int id
 * @property int profile_id
 * @property string title
 *
 * @package Yeltrik\Profile\app\models
 */
class CorporateTitle extends Model
{
    use HasFactory;

    protected $connection = 'profile';
    public $table = 'corporate_titles';

    /**
     * @return CorporateTitleFactory
     */
    public static function newFactory()
    {
        return new CorporateTitleFactory();
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
