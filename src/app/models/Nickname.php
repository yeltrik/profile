<?php

namespace Yeltrik\Profile\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\Profile\database\factories\NicknameFactory;

/**
 * Class Nickname
 *
 *
 * @package Yeltrik\Profile\app\models
 */
class Nickname extends Model
{
    use HasFactory;

    protected $connection = 'profile';
    public $table = 'nicknames';

    /**
     * @return NicknameFactory
     */
    public static function newFactory()
    {
        return new NicknameFactory();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

}
