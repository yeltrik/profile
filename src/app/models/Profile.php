<?php

namespace Yeltrik\Profile\app\models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yeltrik\PdPSR\app\models\ProgramTag;
use Yeltrik\Profile\database\factories\ProfileFactory;

/**
 * Class Profile
 *
 * @property int id
 *
 * @property Collection corporateTitles
 * @property Collection emails
 * @property Collection nicknames
 * @property Collection personalNames
 * @property Collection tags
 *
 * @package Yeltrik\Profile\app\models
 */
class Profile extends Model
{
    use HasFactory;

    protected $connection = 'profile';
    public $table = 'profiles';

    /**
     * @return HasMany
     */
    public function corporateTitles()
    {
        return $this->hasMany(CorporateTitle::class);
    }

    /**
     * @return HasMany
     */
    public function emails()
    {
        return $this->hasMany(Email::class);
    }

    /**
     * @return ProfileFactory
     */
    public static function newFactory()
    {
        return new ProfileFactory();
    }

    /**
     * @return HasMany
     */
    public function nicknames()
    {
        return $this->hasMany(Nickname::class);
    }

    /**
     * @return HasMany
     */
    public function personalNames()
    {
        return $this->hasMany(PersonalName::class);
    }

    /**
     * @return HasMany
     */
    public function tags()
    {
        return $this->hasMany(ProgramTag::class);
    }

}
