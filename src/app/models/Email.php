<?php

namespace Yeltrik\Profile\app\models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yeltrik\Profile\database\factories\EmailFactory;

/**
 * Class Email
 *
 * @property int id
 * @property int profile_id
 * @property string email
 *
 * @property Profile profile
 * @property Collection emailTags
 *
 * @package Yeltrik\Profile\app\models
 */
class Email extends Model
{
    use HasFactory;

    protected $connection = 'profile';
    public $table = 'emails';

    /**
     * @return EmailFactory
     */
    public static function newFactory()
    {
        return new EmailFactory();
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    /**
     * @return HasMany
     */
    public function emailTags()
    {
        return $this->hasMany(EmailTag::class);
    }

}
