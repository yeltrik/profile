<?php


namespace Yeltrik\Profile\app;


use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;

class UserProfile
{

    private User $user;

    /**
     * UserProfile constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Builder
     */
    private function emailsQuery()
    {
        return Email::query()
            ->where('email', '=', $this->userEmailString()
            );
    }

    /**
     * @param bool $create
     * @return Profile|null
     */
    public function profile(bool $create = FALSE)
    {
        $profile = $this->profileQuery()->get()->first();
        if ($profile instanceof Profile === FALSE && $create === TRUE) {
            $profile = new Profile();
            $profile->save();
        }

        if ($profile instanceof Profile === FALSE && $create === FALSE) {
            return NULL;
        }

        $foundEmail = FALSE;
        if ($profile->emails->count() > 0) {
            foreach ($profile->emails as $email) {
                if ($email->email === $this->userEmailString()) {
                    $foundEmail = TRUE;
                }
            }
        }
        if ($foundEmail === FALSE) {
            $email = new Email();
            $email->profile()->associate($profile);
            $email->email = $this->userEmailString();
            $email->save();
        }

//        $personalName = PersonalName::query()
//            ->where('first', '=', $first)
//            ->where('last', '=', $last)
//            ->first();
        
//        $foundPersonalName = FALSE;
//        $first = $this->userFirstNameString();
//        $last = $this->userLastNameString();
//        if ($first != NULL && $last != NULL) {
//            if ($profile->personalNames->count() > 0) {
//                foreach ($profile->personalNames as $personalName) {
//                    if (
//                        $personalName->first === $first &&
//                        $personalName->last === $last
//                    ) {
//                        $foundPersonalName = TRUE;
//                    }
//                }
//            }
//            if ($foundPersonalName === FALSE) {
//                $personalName = new PersonalName();
//                $personalName->profile()->associate($profile);
//                $personalName->first = $first;
//                $personalName->last = $last;
//                $personalName->save();
//            }
//        }

        return $profile;
    }

    /**
     * @return Builder
     */
    private function profileQuery()
    {
        return Profile::query()
            ->whereIn('id', $this->emailsQuery()
                ->pluck('profile_id')
                ->toArray()
            );
    }

    /**
     * @return mixed
     */
    private function userEmailString()
    {
        return $this->user->email;
    }

    /**
     * @return mixed|string|null
     */
    private function userFirstNameString()
    {
        $name = $this->user->name;
        if ( stristr($name, ',') ) {
            $parts = explode(",", $name);
            return trim($parts[1]);
            // return trim of imploded Array after unsetting first element.
        } elseif ( stristr($name, " ")) {
            $parts = explode(" ", $name);
            return $parts[0];
        } else {
            return NULL;
        }
    }

    /**
     * @return string|null
     */
    private function userLastNameString()
    {
        $name = $this->user->name;
        if ( stristr($name, ',') ) {
            $parts = explode(",", $name);
            return trim($parts[0]);
            // return trim of imploded Array after unsetting first element.
        } elseif ( stristr($name, " ")) {
            $parts = explode(" ", $name);
            array_shift($parts);
            return implode(" ", $parts);
        } else {
            return NULL;
        }
    }

}
