<?php


namespace Yeltrik\Profile\app\import;


use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;

class ProfileImporter
{

    private ?Email $email = NULL;
    private ?string $emailStr = NULL;
    private ?PersonalName $personalName = NULL;
    private ?string $personalNameFirst = NULL;
    private ?string $personalNameLast = NULL;
    private ?Profile $profile = NULL;

    /**
     * @param string $emailStr
     */
    public function associateWithEmailStr(string $emailStr)
    {
        $this->emailStr = strtolower($emailStr);
        $this->email = Email::query()
            ->where('email', '=', $this->emailStr)
            ->first();
    }

    /**
     * @param string $first
     * @param string $last
     */
    public function associateWithPersonalNameStr(string $first, string $last)
    {
        $this->personalNameFirst = ucwords($first);
        $this->personalNameLast = ucwords($last);
        $this->personalName = PersonalName::query()
            ->where('first', '=', $this->personalNameFirst)
            ->where('last', '=', $this->personalNameLast)
            ->first();
    }

    /**
     * @return Profile|null
     */
    private function firstProfileFromDependents()
    {
        // Note sometimes there can be two separate Profiles assigned for dependents, and they should be merged.
        if ($this->email instanceof Email) {
            return $this->email->profile;
        } elseif ($this->personalName instanceof PersonalName) {
            return $this->personalName->profile;
        } else {
            return NULL;
        }
    }

    /**
     * @param bool $force
     * @return Profile|null
     */
    public function profile(bool $force = FALSE): ?Profile
    {
        if ($this->profile instanceof Profile) {
            return $this->profile;
        } else {
            if ( $this->email instanceof Email === FALSE && $this->emailStr != NULL ) {
                $this->email = new Email();
                $this->email->email = $this->emailStr;
            }

            if($this->personalName instanceof PersonalName == FALSE && ($this->personalNameFirst != NULL && $this->personalNameLast != NULL) ) {
                $this->personalName = new PersonalName();
                $this->personalName->first = $this->personalNameFirst;
                $this->personalName->last = $this->personalNameLast;
            }

            $this->profile = $this->firstProfileFromDependents();

            if ( $this->profile instanceof Profile ) {

            } elseif ($this->email instanceof Email || $this->personalName instanceof PersonalName ) {
                $this->profile = new Profile();
                $this->profile->save();
            } elseif ( $force ) {
                $this->profile = new Profile();
                $this->profile->save();
            }

            if ( $this->profile instanceof Profile ) {
                if ( $this->email instanceof Email && $this->email->profile instanceof Profile === FALSE ) {
                    $this->email->profile()->associate($this->profile);
                    $this->email->save();
                }
                if ( $this->personalName instanceof PersonalName && $this->personalName->profile instanceof Profile === FALSE) {
                    $this->personalName->profile()->associate($this->profile);
                    $this->personalName->save();
                }
            }
        }
        return $this->profile;
    }

}
